plugins {
    id("java")
    id("application")
    id("com.github.johnrengelman.shadow") version "8.1.1"
    id("pl.allegro.tech.build.axion-release") version "1.16.1"
}

group = "be.epireyn"
version = "1.0"

repositories {
    mavenCentral()
}

application {
    mainClass = "be.epireyn.amh.Main"
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")

    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.16.0")
}

tasks.test {
    useJUnitPlatform()
}

tasks.shadowJar {
    archiveFileName = "AMH.jar"
}

scmVersion {
    tag {
        prefix = "Release"
    }
}
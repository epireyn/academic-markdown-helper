package be.epireyn.amh;

import be.epireyn.amh.interfaces.ConsoleInterface;
import be.epireyn.amh.manager.ConfigManager;
import be.epireyn.amh.manager.ProjectManager;
import be.epireyn.amh.manager.TaskManager;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;

public class Main {

    public static final File ROOT;

    static {
        try {
            ROOT = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) throws IOException {
        ConsoleInterface consoleInterface = new ConsoleInterface();
        consoleInterface.send("Loading projects and config file...");
        System.out.println("Root directory: " + ROOT.getAbsolutePath());
        ProjectManager.loadProjects(ROOT);
        ProjectManager.loadDefaultConfig(ROOT);
        consoleInterface.send(ProjectManager.getProjects().size() + " projects loaded.");
        if (args.length == 0) {
            TaskManager.runSelectTask(consoleInterface);
        } else {
            String task = args[0];
            if (args.length > 1) {
                args = Arrays.copyOfRange(args, 1, args.length);
            } else {
                args = new String[0];
            }
            TaskManager.runTask(task, args, consoleInterface);
        }
    }
}

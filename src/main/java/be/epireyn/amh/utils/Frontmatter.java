package be.epireyn.amh.utils;

import be.epireyn.amh.config.ConfigProject;
import be.epireyn.amh.manager.ConfigManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.util.Locale;

public class Frontmatter {
    private final JsonNode frontmatter;
    private static final String DATE_PLACEHOLDER = "$today$";

    public Frontmatter(String yaml) throws IOException {
        frontmatter = ConfigManager.MAPPER.readTree(yaml);
    }

    public void parse(ConfigProject config) {
        setValue("title", frontmatter.get("title").asText().replace("$title$", config.getTitle()));
        String dateString = frontmatter.get("date").asText();
        if (dateString.contains(DATE_PLACEHOLDER)) {
            DateTimeFormatter formatter;
            if (frontmatter.hasNonNull("date-format")) {
                if (frontmatter.hasNonNull("lang")) {
                    formatter = DateTimeFormatter.ofPattern(frontmatter.get("date-format").asText(), Locale.forLanguageTag(frontmatter.get("lang").asText()));
                } else {
                    formatter = DateTimeFormatter.ofPattern(frontmatter.get("date-format").asText());
                }
            } else if (frontmatter.hasNonNull("lang")) {
                formatter = DateTimeFormatter
                        .ofLocalizedDateTime(FormatStyle.LONG).localizedBy(Locale.forLanguageTag(frontmatter.get("lang").asText()));
            } else {
                formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
            }
            setValue("date", dateString.replace(DATE_PLACEHOLDER, formatter.format(LocalDateTime.now().atZone(ZoneId.systemDefault()))));
        }
    }

    private void setValue(String key, String value) {
        ((ObjectNode) frontmatter).put(key, value);
    }

    @Override
    public String toString() {
        try {
            return ConfigManager.MAPPER.writeValueAsString(frontmatter).replace(" null", "");
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}

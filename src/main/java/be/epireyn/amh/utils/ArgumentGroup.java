package be.epireyn.amh.utils;

import java.util.List;

public class ArgumentGroup {
    private final List<Argument> arguments;

    public ArgumentGroup(Argument... arguments) {
        this.arguments = List.of(arguments);
    }

    public List<Argument> getArguments() {
        return arguments;
    }
}

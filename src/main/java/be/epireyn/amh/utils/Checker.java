package be.epireyn.amh.utils;

import be.epireyn.amh.interfaces.Interface;

import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public final class Checker {

    public static int askUntilInteger(Interface face, String initialPrompt, int min, int max) {
        var ref = new Object() {
            Function<String, Integer> function = null;
        };
        ref.function = s -> {
            int result;
            Object prompt = face.prompt(s);
            if (prompt instanceof Integer) {
                result = (int) prompt;
            } else {
                String promptS = String.valueOf(prompt);
                try {
                    result = Integer.parseInt(promptS);
                } catch (NumberFormatException e) {
                    return ref.function.apply("Your answer must be a number.\n");
                }
            }
            if (result < min || result > max) {
                return ref.function.apply("Your answer must be in the range " + min + " - " + max + ". \n");
            }
            return result;
        };
        return ref.function.apply(initialPrompt);
    }

    public static String askUntilValidString(Interface face, String initialPrompt, String regex) {
        Predicate<String> predicate = Pattern.compile(regex).asMatchPredicate();
        String result;
        String prompt = initialPrompt;
        do {
            result = String.valueOf(face.prompt(prompt));
            if (predicate.test(result)) {
                return result;
            }
            prompt = "Please write a name that matches this regex: `" + regex + "`.\n";
        } while (true);
    }

    public static File askUntilValidFile(Interface face, String initialPrompt, boolean mustExist, boolean canBeBlank) {
        File file = null;
        do {
            try {
                String response = String.valueOf(face.prompt(initialPrompt));
                if (response == null || response.isBlank() && canBeBlank) {
                    return null;
                }
                file = Path.of(response).toFile();
                if (mustExist && !file.exists()) {
                    initialPrompt = "File does not exist. Create it or choose another before trying again.\n";
                    file = null;
                }
            } catch (InvalidPathException e) {
                initialPrompt = "Invalid file path format. Please check the path.\n";
            }
        } while (file == null);
        return file;
    }
}

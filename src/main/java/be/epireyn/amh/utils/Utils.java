package be.epireyn.amh.utils;

import java.io.File;

public final class Utils {

    public static final void deleteFolder(File folder) {
        if (folder.listFiles() != null) {
            for (File file : folder.listFiles()) {
                file.delete();
            }
        }
        folder.delete();
    }
}

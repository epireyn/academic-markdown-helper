package be.epireyn.amh.tasks;

import be.epireyn.amh.config.ConfigProject;
import be.epireyn.amh.interfaces.Interface;
import be.epireyn.amh.manager.ConfigManager;
import be.epireyn.amh.manager.ProjectManager;
import be.epireyn.amh.project.Project;
import be.epireyn.amh.utils.Argument;
import be.epireyn.amh.utils.ArgumentGroup;
import be.epireyn.amh.utils.Checker;
import be.epireyn.amh.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

public class BuildTask extends Task {

    public BuildTask() {
        super("Build", "Build project (create a PDF document from markdown)",
                new ArgumentGroup(
                        new Argument("Project directory", false)
                ));
    }

    @Override
    public void run(Interface face, String[] args) {
        Path projectRoot;
        if (args.length == 0) {
            projectRoot = selectProject(face).getRoot().toPath();
        } else {
            projectRoot = Path.of(args[0]);
        }
        File projectRootFile = projectRoot.toFile();
        if (!projectRootFile.exists() || !ConfigManager.containsConfig(projectRootFile)) {
            face.send("No project was found at the path " + projectRoot.toAbsolutePath());
            return;
        }
        build(face, projectRootFile);
    }

    private Project selectProject(Interface face) {
        File currentProject = Path.of(System.getProperty("user.dir")).toFile();
        boolean isProject = ProjectManager.isProject(currentProject);
        List<Project> projects = ProjectManager.getProjects();
        int index = isProject ? 1 : 0;
        StringBuilder builder = new StringBuilder();
        builder.append(0).append(") ").append("Custom path").append("\n");
        if (isProject) {
            builder.append(1).append(") ").append("Current folder").append(" - ").append(currentProject).append("\n");
        }

        if (!isProject && projects.isEmpty()) {
            return customProject(face);
        }
        if (projects.isEmpty()) {
            builder.append("No projects contained in this folder.").append("\n");
        } else {
            for (Project project : projects) {
                builder.append(++index).append(") ").append(project.getTitle()).append(" - ").append(project.getConfigManager().getFolder().getAbsolutePath()).append("\n");
            }
        }
        int projectIndex = Checker.askUntilInteger(face, String.valueOf(builder), 0, index);
        if (projectIndex == 0) {
            return customProject(face);
        }
        if (isProject && projectIndex == 1) {
            try {
                return new Project(currentProject, new ConfigManager(currentProject));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return ProjectManager.getProjects().get(projectIndex - (isProject ? 2 : 1));
    }

    private Project customProject(Interface face) {
        Project project = null;
        String prompt = "Select path to custom project root folder. If blank, closes the program:";
        do {
            File file = Checker.askUntilValidFile(face, prompt, true, true);
            if (file == null) {
                System.exit(0);
            } else {
                if (ProjectManager.isProject(file)) {
                    try {
                        project = new Project(file, new ConfigManager(file));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        } while (project == null);
        return project;
    }

    private void build(Interface face, File projectRootFile) {
        try {
            ConfigManager manager = new ConfigManager(projectRootFile);
            ConfigProject project = manager.getConfig().getProject();
            String name = project.getTitle();
            assert name != null;
            File outputDirectory = new File(projectRootFile, "out");
            File markdown = new File(projectRootFile, name + ".md");
            File pdfOutput = null;

            if (outputDirectory.exists()) {
                Utils.deleteFolder(outputDirectory);
            }
            outputDirectory.mkdir();

            if (project.getOutputFolderForPdf() != null) {
                pdfOutput = projectRootFile.toPath().resolve(manager.getConfig().getProject().getOutputFolderForPdf()).toFile();
            }
            File metadata = null;
            if (project.getDefaultPandocMetadata() != null) {
                metadata = projectRootFile.toPath().resolve(manager.getConfig().getProject().getDefaultPandocMetadata()).toFile();
            }
            File template = null;
            if (project.getTemplate() != null) {
                template = projectRootFile.toPath().resolve(manager.getConfig().getProject().getTemplate()).toFile();
            }
            String extensions = "";
            if (project.getPandocMarkdownExtensions() != null) {
                extensions = String.join("+", project.getPandocMarkdownExtensions());
            }

            File texOutput = new File(outputDirectory, name + ".tex");
            face.send("Starting pandoc...");
            pandoc(template, metadata, project.isNumberedSections(), extensions, texOutput, markdown).waitFor();

            if (!texOutput.exists()) {
                face.send("Missing output file. Please try again and fix errors shown above.");
                return;
            } else {
                face.send("Pandoc succeeded.");
            }
            face.send("Starting first compilation with xelatex...");

            xelatex(projectRootFile, outputDirectory, name, texOutput).waitFor();

            File auxOutput = new File(outputDirectory, name + ".aux");
            if (!auxOutput.exists()) {
                face.send("Missing output file. Please try again or fix errors shown above.");
            } else {
                face.send("Xelatex succeeded.");
            }
            face.send("Running citeproc-lua...");
            citeproc(projectRootFile, auxOutput).waitFor();
            face.send("Running xelatex twice...");
            for (int i = 0; i < 2; i++) {
                xelatex(projectRootFile, outputDirectory, name, texOutput).waitFor();
            }
            if (pdfOutput != null) {
                if (!pdfOutput.exists()) {
                    pdfOutput.mkdirs();
                }
                pdfOutput = new File(pdfOutput, name + ".pdf");
                Files.copy(outputDirectory.toPath().resolve(name + ".pdf"), pdfOutput.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } else {
                pdfOutput = new File(outputDirectory, name + ".pdf");
            }
            face.send("All processes haven been run (check for any errors above).\n" +
                    "If succeeded, the PDF document is stored in " + pdfOutput.getAbsolutePath());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private Process xelatex(File rootFolder, File outputDirectory, String name, File texOutput) throws InterruptedException, IOException {
        String[] command = {"xelatex",
                "-output-directory=" +
                        rootFolder.toPath().relativize(outputDirectory.toPath()),
                "-jobname=" + name,
                rootFolder.toPath().relativize(texOutput.toPath()).toString()};
        ProcessBuilder xelatex = new ProcessBuilder().command(command).directory(rootFolder);
        xelatex.inheritIO();
        return xelatex.start();
    }

    private Process pandoc(File template, File metadata, boolean numberedSections, String extensions, File texOutput, File input) throws IOException {
        ProcessBuilder pandoc = new ProcessBuilder();
        extensions = "markdown+citations+raw_tex" + (!extensions.isBlank() ? "+" + extensions : "");
        List<String> command = new ArrayList<>(List.of("pandoc", input.getAbsolutePath(), "-s", "-F", "pandoc-crossref", "--biblatex", "-f", extensions, "-t", "latex", "-o", texOutput.getAbsolutePath()));

        if (template != null) {
            command.add("--template=" + template.getAbsolutePath());
        }
        if (metadata != null) {
            command.add("--metadata=" + metadata.getAbsolutePath());
        }
        if (numberedSections) {
            command.add("-N");
        }
        pandoc.command(command);
        pandoc.inheritIO();
        return pandoc.start();
    }

    private Process citeproc(File rootDir, File auxOutput) throws IOException {
        List<String> cmd = new ArrayList<>();
        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            String userHome = System.getProperty("user.home");

            Path citeproc = Path.of(userHome).resolve("AppData/Local/Programs/MiKTeX/scripts/citation-style-language/citeproc-lua.lua");
            if (!citeproc.toFile().exists()) {
                throw new IllegalStateException("citeproc-lua.lua wasn't found in " + citeproc);
            }
            cmd.add("texlua");
            cmd.add(citeproc.toString());
        } else {
            cmd.add("citeproc-lua");
        }
        cmd.add(auxOutput.getAbsolutePath());
        ProcessBuilder citeproc = new ProcessBuilder().command(cmd).directory(rootDir);
        citeproc.inheritIO();
        return citeproc.start();
    }

}

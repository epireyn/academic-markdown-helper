package be.epireyn.amh.tasks;

import be.epireyn.amh.interfaces.Interface;
import be.epireyn.amh.manager.TaskManager;
import be.epireyn.amh.utils.Checker;

public class SelectTask extends Task {
    public SelectTask() {
        super("select", "Select a task");
    }

    @Override
    public void run(Interface face, String[] args) {
        StringBuilder builder = new StringBuilder("These are the available tasks. Please choose one (1 - ")
                .append(TaskManager.getTasks().size()).append(").\n");
        int index = 0;
        for (Task task : TaskManager.getTasks()) {
            builder.append(getTaskLine(++index, task.getName(), task.getDescription())).append("\n");
        }
        builder.append(getTaskLine(0, "Exit", "Close this program.")).append("\n");
        builder.append("\nChosen index: ");
        int taskIndex = Checker.askUntilInteger(face, builder.toString(), 0, TaskManager.getTasks().size());
        if (taskIndex == 0) {
            System.exit(0);
        } else {
            TaskManager.getTasks().get(taskIndex - 1).run(face, new String[0]);
        }
    }

    private String getTaskLine(int index, String name, String description) {
        return index + " - " + name + ": " + description;
    }
}

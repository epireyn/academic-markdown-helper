package be.epireyn.amh.tasks;

import be.epireyn.amh.interfaces.Interface;
import be.epireyn.amh.manager.TaskManager;

public class HelpTask extends Task {

    public HelpTask() {
        super("Help", "Show the help message.");
    }

    @Override
    public void run(Interface face, String[] args) {
        if (args.length == 0) {
            StringBuilder builder = new StringBuilder("Available tasks");
            for (Task task : TaskManager.getTasks()) {
                builder.append("\n").append(buildEntry(task));
            }
            face.send(builder.toString());
        } else {
            var res = TaskManager.getTasks().stream().filter(task -> task.getName().equalsIgnoreCase(args[0])).findFirst();
            if (res.isPresent()) {
                face.send(buildEntry(res.get()));
            } else {
                face.send("Unknown task `" + args[0] + "`.");
            }
        }
    }

    private String buildEntry(Task task) {
        StringBuilder builder = new StringBuilder();
        builder.append("-------- ").append(task.getName()).append(" --------");
        builder.append("\n").append("* ").append(task.getDescription());
        builder.append("\n").append("* Usage: ").append(task.buildHelp());
        builder.append("\n");
        int offset = 9 * 2;
        builder.append("-".repeat(Math.max(0, offset + task.getName().length())));
        return builder.toString();
    }
}

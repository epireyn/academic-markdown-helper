package be.epireyn.amh.tasks;

import be.epireyn.amh.Main;
import be.epireyn.amh.config.ConfigProject;
import be.epireyn.amh.interfaces.Interface;
import be.epireyn.amh.manager.ProjectManager;
import be.epireyn.amh.project.Project;
import be.epireyn.amh.utils.Argument;
import be.epireyn.amh.utils.ArgumentGroup;
import be.epireyn.amh.utils.Checker;
import be.epireyn.amh.utils.Frontmatter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Pattern;

public class CreateTask extends Task {

    private static final String REGEX_FOR_NAME = "[^:/\\\\*^,;\"']+";

    public CreateTask() {
        super("Create", "Create a new project.",
                new ArgumentGroup(
                        new Argument("Project name", true)
                ),
                new ArgumentGroup(
                        new Argument("PDF output folder", false)
                ),
                new ArgumentGroup(
                        new Argument("Markdown template file", false)
                ));
    }

    @Override
    public void run(Interface face, String[] args) {
        String name;
        if (args.length == 0) {
            name = Checker.askUntilValidString(face, "Project name: ", REGEX_FOR_NAME);
        } else {
            if (!Pattern.compile(REGEX_FOR_NAME).asMatchPredicate().test(args[0])) {
                name = Checker.askUntilValidString(face, "Invalid name. It should respect this regex `" + REGEX_FOR_NAME + "`. Try again:\n", REGEX_FOR_NAME);
            } else {
                name = args[0];
            }
        }
        Project project;
        try {
            project = ProjectManager.createNewProject(Main.ROOT, name, false);
        } catch (IllegalArgumentException e) {
            String prompt = Checker.askUntilValidString(face, "A project with the same name already exists. Do you want to overwrite it? (overwrite/cancel)\n", "(overwrite)|(cancel)");
            if (prompt.equals("overwrite")) {
                try {
                    project = ProjectManager.createNewProject(Main.ROOT, name, true);
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            } else {
                face.send("No project created.");
                return;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        face.send("You can configure the output folder for the future generated pdf document.");
        Path path = promptForPath(face, project.getRoot());
        project.getConfigManager().getConfig().getProject().setOutputFolderForPdf(path.toString());
        project.getConfigManager().save(project.getConfigManager().getConfig());

        String markdownTemplate = ProjectManager.getDefaultConfig().getConfig().getMarkdownTemplate();
        boolean hasTemplate = markdownTemplate != null && !markdownTemplate.isBlank();
        Path markdownTemplatePath = null;
        if (hasTemplate) {
            markdownTemplatePath = Path.of(ProjectManager.getDefaultConfig().getFolder().getAbsolutePath(), markdownTemplate);
            hasTemplate = markdownTemplatePath.toFile().exists();
        }
        face.send("You can choose the template of the markdown file. A blank input will result in " + (hasTemplate ? "keeping the template of `" + ProjectManager.getDefaultConfig().getFileName() + "`" : "an empty file") + ".\n");
        File template = Checker.askUntilValidFile(face, "Markdown template: ", true, true);
        if (template == null && hasTemplate) {
            template = markdownTemplatePath.toFile();
        }
        if (template != null) {
            if (copyTemplate(template, project.getMarkdown(), project.getConfigManager().getConfig().getProject())) {
                face.send("Template successfully copied.");
            } else {
                face.send("Template was not found or an error occurred.");
            }
        }

        face.send("Project created!");
    }

    private Path promptForPath(Interface face, File projectRoot) {
        String keyword = "%path%";
        String prompt = String.valueOf(face.prompt("An blank input will keep the default path written in`" +
                ProjectManager.getDefaultConfig().getFileName() + "`. An input starting with `" + keyword + "` will be interpreted as such as well.\n" +
                "Example: `" + keyword + "/out` will create a sub-folder `out` in the default path.\n"));
        Path path;
        String output = ProjectManager.getDefaultConfig().getConfig().getProject().getOutputFolderForPdf();
        Path defaultPath = output == null ? projectRoot.toPath() : Path.of(output);
        if (prompt == null || prompt.isBlank()) {
            path = defaultPath.resolve("out");
        } else {
            prompt = prompt.trim();

            if (prompt.startsWith(keyword)) {
                prompt = prompt.replace(keyword, "");
                while (prompt.startsWith("/")) {
                    prompt = prompt.substring(1);
                }
                path = defaultPath.resolve(prompt);
            } else {
                path = Path.of(prompt);
            }
        }
        return path;
    }


    private static boolean copyTemplate(File from, File to, ConfigProject configProject) {
        if (!from.exists() || Files.isDirectory(from.toPath())) {
            return false;
        }
        try{
            FileReader fr = new FileReader(from);
            String s;
            StringBuilder metaBuilder = new StringBuilder();
            StringBuilder contentBuilder = new StringBuilder();
            boolean meta = false;
            try (BufferedReader br = new BufferedReader(fr)) {
                while ((s = br.readLine()) != null) {
                    if (s.equalsIgnoreCase("---") || (meta && s.equalsIgnoreCase("..."))) {
                        meta = !meta;
                    } else if (meta) {
                        metaBuilder.append(s).append("\n");
                    } else {
                        contentBuilder.append(s).append("\n");
                    }
                }
                Frontmatter frontmatter = new Frontmatter(metaBuilder.toString());
                frontmatter.parse(configProject);
                metaBuilder = new StringBuilder();
                metaBuilder.append("---\n").append(frontmatter).append("...\n");
                contentBuilder.insert(0, metaBuilder);
                try (FileWriter writer = new FileWriter(to)) {
                    writer.write(contentBuilder.toString());
                }
            }
            return true;
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }
}

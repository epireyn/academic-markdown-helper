package be.epireyn.amh.tasks;

import be.epireyn.amh.interfaces.Interface;
import be.epireyn.amh.utils.Argument;
import be.epireyn.amh.utils.ArgumentGroup;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class Task {

    private final String name;
    private final String description;
    private final List<ArgumentGroup> arguments;

    public Task(String name, String description, ArgumentGroup... arguments) {
        this.name = name;
        this.description = description;
        this.arguments = List.of(arguments);
    }

    public abstract void run(Interface face, String[] args);

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<ArgumentGroup> getArguments() {
        return arguments;
    }

    public String buildHelp() {
        if (arguments == null || arguments.isEmpty()) {
            return name.toLowerCase();
        }
        StringBuilder builder = new StringBuilder(name.toLowerCase());
        List<ArgumentGroup> arguments = new ArrayList<>(this.arguments);
        arguments.sort(Comparator.comparingInt(value -> value.getArguments().size()));

        for (ArgumentGroup group : arguments) {
            if (group.getArguments().size() == 1) {
                builder.append(" ");
                buildArgument(group.getArguments().get(0), builder);
            } else {
                builder.append(" <");
                for (Argument argument : group.getArguments()) {
                    buildArgument(argument, builder).append("|");
                }
                builder.delete(builder.length() - 1, builder.length()).append(">");
            }
        }
        return builder.toString();
    }

    private StringBuilder buildArgument(Argument argument, StringBuilder builder) {
        if (!argument.isRequired()) {
            builder.append("[");
        }
        builder.append(argument.getName().replace(" ", "_").toLowerCase());
        if (!argument.isRequired()) {
            builder.append("]");
        }
        return builder;
    }
}

package be.epireyn.amh.tasks;

import be.epireyn.amh.interfaces.Interface;
import be.epireyn.amh.utils.Argument;
import be.epireyn.amh.utils.ArgumentGroup;

import java.io.File;
import java.nio.file.Path;

public class CleanTask extends Task {

    public CleanTask() {
        super("Clean", "Clean output folder and files", new ArgumentGroup(
                new Argument("Project directory", false)
        ));
    }

    @Override
    public void run(Interface face, String[] args) {
        Path projectRoot;
        if (args.length == 0) {
            projectRoot = Path.of(System.getProperty("user.dir"));
        } else {
            projectRoot = Path.of(args[1]);
        }
        File projectRootFile = projectRoot.toFile();
        if (!projectRootFile.exists()) {
            face.send("No project was found at the path " + projectRoot.toAbsolutePath());
            return;
        }
        File file = new File(projectRootFile, "out");
        if (file.delete()) {
            face.send("Project cleaned.");
        } else {
            face.send("No file to clean.");
        }
    }
}

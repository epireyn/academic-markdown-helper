package be.epireyn.amh.config;

public class Config {

    private String markdownTemplate;
    private ConfigProject project;

    public Config() {
        project = new ConfigProject();
    }

    public ConfigProject getProject() {
        return project;
    }

    public void setProject(ConfigProject project) {
        this.project = project;
    }

    public String getMarkdownTemplate() {
        return markdownTemplate;
    }

    public void setMarkdownTemplate(String markdownTemplate) {
        this.markdownTemplate = markdownTemplate;
    }
}

package be.epireyn.amh.config;

import java.util.List;

public class ConfigProject {

    private String title;
    private String outputFolderForPdf;
    private String defaultPandocMetadata;
    private String template;
    private boolean numberedSections;
    private List<String> pandocMarkdownExtensions;

    public ConfigProject() {
    }

    public String getTitle() {
        return title;
    }

    public String getOutputFolderForPdf() {
        return outputFolderForPdf;
    }

    public String getDefaultPandocMetadata() {
        return defaultPandocMetadata;
    }

    public String getTemplate() {
        return template;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setOutputFolderForPdf(String outputFolderForPdf) {
        this.outputFolderForPdf = outputFolderForPdf;
    }

    public void setDefaultPandocMetadata(String defaultPandocMetadata) {
        this.defaultPandocMetadata = defaultPandocMetadata;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public boolean isNumberedSections() {
        return numberedSections;
    }

    public void setNumberedSections(boolean numberedSections) {
        this.numberedSections = numberedSections;
    }

    public List<String> getPandocMarkdownExtensions() {
        return pandocMarkdownExtensions;
    }

    public void setPandocMarkdownExtensions(List<String> pandocMarkdownExtensions) {
        this.pandocMarkdownExtensions = pandocMarkdownExtensions;
    }
}

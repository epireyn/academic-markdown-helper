package be.epireyn.amh.manager;

import be.epireyn.amh.interfaces.Interface;
import be.epireyn.amh.tasks.*;

import java.util.List;

public final class TaskManager {
    private static final List<Task> TASKS = List.of(new HelpTask(), new BuildTask(), new CleanTask(), new CreateTask());

    public static List<Task> getTasks() {
        return TASKS;
    }

    public static void runTask(String name, String[] args, Interface face) {
        var match = TASKS.stream().filter(task -> task.getName().equalsIgnoreCase(name)).findFirst();
        Task task = match.orElse(null);
        if (task == null) {
            task = new HelpTask();
        }
        task.run(face, args);
    }

    public static void runSelectTask(Interface face) {
        new SelectTask().run(face, new String[0]);
    }
}

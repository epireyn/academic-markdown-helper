package be.epireyn.amh.manager;

import be.epireyn.amh.config.ConfigProject;
import be.epireyn.amh.project.Project;
import be.epireyn.amh.utils.Frontmatter;
import be.epireyn.amh.utils.Utils;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectManager {

    private static List<Project> projects;
    private static ConfigManager defaultConfig;

    public static void loadProjects(File root) throws IOException {
        assert Files.isDirectory(root.toPath());
        projects = new ArrayList<>();
        File[] folders = root
                .listFiles(pathname -> Files.isDirectory(pathname.toPath()));
        if (folders != null) {
            for (File projectFolder : folders) {
                if (ConfigManager.containsConfig(projectFolder)) {
                    projects.add(new Project(projectFolder, new ConfigManager(projectFolder)));
                }
            }
        }
    }

    public static ConfigManager loadDefaultConfig(File root) throws IOException {
        return defaultConfig = new ConfigManager(root, "default-configuration.yml");
    }

    public static ConfigManager getDefaultConfig() {
        if (defaultConfig == null) {
            throw new IllegalStateException("Default config has not been loaded yet!");
        }
        return defaultConfig;
    }

    public static List<Project> getProjects() {
        if (projects == null) {
            throw new IllegalStateException("Projects have not been loaded yet!");
        }
        return projects;
    }

    public static Project createNewProject(File root, String name, boolean force) throws IOException {
        File project = new File(root, name);
        if (project.exists()) {
            if (force) {
                Utils.deleteFolder(project);
            } else {
                throw new IllegalArgumentException("Project already exists.");
            }
        }
        project.mkdir();
        ConfigManager manager = new ConfigManager(project);
        manager.copy(ProjectManager.getDefaultConfig());

        ConfigProject config = manager.getConfig().getProject();
        config.setTitle(name);
        manager.save(manager.getConfig());

        File markdown = new File(project, name + ".md");
        markdown.createNewFile();

        Project p = new Project(project, manager);
        projects.add(p);
        return p;
    }

    public static boolean isProject(File folder) {
        return ConfigManager.containsConfig(folder);
    }
}

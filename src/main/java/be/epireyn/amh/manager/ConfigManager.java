package be.epireyn.amh.manager;

import be.epireyn.amh.config.Config;
import be.epireyn.amh.config.ConfigProject;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class ConfigManager {

    private static final String CONFIG_NAME = "config.yml";
    public static final ObjectMapper MAPPER = new ObjectMapper(new YAMLFactory().disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER)).setSerializationInclusion(JsonInclude.Include.NON_NULL);
    private final File config;

    private Config loadedConfig;

    public ConfigManager(File root, String configName) throws IOException {
        assert root.exists() && Files.isDirectory(root.toPath());
        config = new File(root, configName);
        if (!config.exists()) {
            System.out.println("Creating config file: " + config.getAbsolutePath());
            save(new Config());
        }
    }

    public ConfigManager(File root) throws IOException {
        this(root, CONFIG_NAME);
    }

    public Config getConfig() {
        try {
            return loadedConfig == null ? load() : loadedConfig;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Config load() throws IOException {
        return loadedConfig = MAPPER.readValue(config, Config.class);
    }

    public void save(Config config) {
        try {
            if (!this.config.exists()) {
                this.config.createNewFile();
            }
            MAPPER.writeValue(this.config, config);
            this.loadedConfig = config;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void copy(ConfigManager from) {
        ConfigProject project = getConfig().getProject();
        ConfigProject fromProject = from.getConfig().getProject();
        if (fromProject.getTemplate() != null) {
            project.setTemplate(new File(config.toPath().getParent().relativize(from.config.toPath().getParent()).toString(), Path.of(fromProject.getTemplate()).getFileName().toString()).getPath());
        }
        if (fromProject.getDefaultPandocMetadata() != null) {
            project.setDefaultPandocMetadata(new File(config.toPath().getParent().relativize(from.config.toPath().getParent()).toString(), Path.of(fromProject.getDefaultPandocMetadata()).getFileName().toString()).getPath());
        }
        if (fromProject.getOutputFolderForPdf() != null) {
            project.setOutputFolderForPdf(new File(config.toPath().getParent().relativize(from.config.toPath().getParent()).toString(), Path.of(fromProject.getOutputFolderForPdf()).getFileName().toString()).getPath());
        }
        save(loadedConfig);
    }

    public String getFileName() {
        return config.getName();
    }

    public File getFolder() {
        return config.getParentFile();
    }

    public static boolean containsConfig(File folder) {
        return new File(folder, CONFIG_NAME).exists();
    }
}

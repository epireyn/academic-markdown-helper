package be.epireyn.amh.interfaces;

public abstract class Interface {

    public abstract Object prompt(String prompt);
    public abstract void send(Object object);

}

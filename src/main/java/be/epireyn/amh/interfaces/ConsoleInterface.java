package be.epireyn.amh.interfaces;

import java.util.Scanner;

public class ConsoleInterface extends Interface {
    private final Scanner scanner = new Scanner(System.in);

    @Override
    public String prompt(String prompt) {
        System.out.print(prompt);
        return scanner.nextLine();
    }

    @Override
    public void send(Object object) {
        System.out.println(object);
    }
}

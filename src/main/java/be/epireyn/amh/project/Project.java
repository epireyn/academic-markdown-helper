package be.epireyn.amh.project;

import be.epireyn.amh.manager.ConfigManager;

import java.io.File;

public class Project {
    private final ConfigManager configManager;
    private final File root;

    public Project(File root, ConfigManager configManager) {
        this.root = root;
        this.configManager = configManager;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public File getRoot() {
        return root;
    }

    public String getTitle() {
        return getConfigManager().getConfig().getProject().getTitle();
    }

    public File getMarkdown() {
        return new File(root, getConfigManager().getConfig().getProject().getTitle() + ".md");
    }
}

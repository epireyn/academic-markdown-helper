---
title: %title%
csl: CSS_STYLE
author:
  - 
bibliography:
cover-page: true
biblio-title:
author-right: false
date: %today%
date-format:
mainfont: Times New Roman
affiliation:
lang: en-GB
geometry: margin=2cm
csquotes: true
linestretch:  1.5
linestretch-quote: 1
...